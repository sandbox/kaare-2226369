;(function($, Drupal) {

Drupal.behaviors.savaxNodeTypeForm = {
  attach: function(context) {
    $('fieldset#edit-savax', context).drupalSetSummary(function(context) {
      var vals = [];
      vals.push($('[name="savax_enabled"]:checked', context).val() == 1 ? 'Enabled' : 'Disabled');
      return vals.join(', ');
    });
  }
};

})(jQuery, Drupal);
