/**
 * @file
 * Disable CKeditor completely for forms with auto save disabled.
 */

;(function($) {

Drupal.savax = Drupal.savax || {};

Drupal.savax.behaviors.ckeditor = {
  attach: function(savax) {
    savax.ckeditor = {
      editors: {}
    };
  },

  update: function(savax, property) {
    if (property == 'enabled' && savax.halted) {
      for (var id in savax.ckeditor.editors) {
        savax.ckeditor.editors[id].setReadOnly();
      }
    }
  }
};

})(jQuery);
