/**
 * @file
 * Ckeditor plugin with integration with savax.module
 */

;(function($) {

CKEDITOR.plugins.add('savax', {

  /**
   * Ckeditor callback.
   *
   * Register this editor with the form's savax object if present.
   */
  init: function(editor) {
    var savax = $(editor.element.$.form).data('savax');
    if (!savax) {
      return;
    }
    // Simple test if we're loaded from wysiwyg.module. In that case we have to
    // bubble up some events in the ckeditor to the designated text area.
    if (Drupal.wysiwyg != undefined) {
      editor.on('change', function(ev) {
        $(ev.editor.element.$).trigger('change');
      });
      editor.on('blur', function(ev) {
        $(ev.editor.element.$).trigger('blur');
      });
      editor.on('focus', function(ev) {
        $(ev.editor.element.$).trigger('click');
      });
    }
    savax.ckeditor.editors[editor.name] = editor;
  }
});

})(jQuery);
