<?php

/**
 * @file
 * Admin page for setting up ajax auto save per workflow state.
 */

/**
 * Implements hook_states_form().
 */
function savax_workflow_states_form($form, &$form_state, WorkflowState $workflow) {
  $wid = $workflow->wid;
  $form['savax_workflow_enabled__' . $wid] = array(
    '#type' => 'radios',
    '#title' => t("Enable auto save only for selected states"),
    '#options' => array(1 => t('Yes'), 0 => t('No')),
    '#description' => t("Default behavior is to auto save independant of workflow status. By enabling this you can select what workflow states auto save will be enabled in."),
    '#default_value' => variable_get('savax_workflow_enabled__' . $wid, 0),
  );
  $states = $workflow->getStates(TRUE);
  $options = array();
  foreach ($states as $state) {
    $options[$state->sid] = $state->label();
  }
  $form['savax_workflow_states__' . $wid] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => t("State"),
    '#default_value' => variable_get('savax_workflow_states__' . $wid, array()),
    '#states' => array(
      'visible' => array(':input[name="savax_workflow_enabled__' . $wid . '"]' => array('value' => 1)),
    ),
  );
  return system_settings_form($form);
}
