/**
 * @file
 * Ajax auto save status bar admin settings.
 */

;(function($) {

Drupal.behaviors.savax_status_admin = {
  attach: function(context) {
    $('fieldset#edit-statusbar-items', context).drupalSetSummary(function(context) {
      var vals = [],
          all = true;
      $('#savax-status-bar-items tbody tr', context).each(function() {
        if ($(this).find('.status-bar-enabled :checked').length) {
          vals.push(Drupal.settings.savax_status_items[this.id.replace('status-bar-item-', '')].title);
        }
        else {
          all = false;
        }
      });
      return vals.length ? (all ? Drupal.t('All') : vals.join(', ')) : Drupal.t('None');
    });

    $('fieldset#edit-autohide', context).drupalSetSummary(function(context) {
      var vals = [],
          $hide = $(':input[name="savax_status_autohide"]:checked', context);

      vals.push($hide.closest('.form-item').find('label').html());
      if (parseInt($hide.val())) {
        vals.push('Delay: ' + ($('#edit-savax-status-delay-hide', context).val() / 1000) + 's');
      }
      return vals.join(', ');
    });
  }
};

})(jQuery);
