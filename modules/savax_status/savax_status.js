/**
 * @file
 * Client side logic for Ajax auto save status bar.
 */

;(function($) {

Drupal.savax = Drupal.savax || {};
Drupal.savax.behaviors = Drupal.savax.behaviors || {};
Drupal.savax.statusBarItems = Drupal.savax.statusBarItems || {};

Drupal.savax.behaviors.statusBar = {
  attach: function(savax) {
    var item;
    savax.statusBar = new Drupal.statusBar({
      autohide: Drupal.settings.savax_status.autohide,
      delay_hide: Drupal.settings.savax_status.delay_hide
    });
    $('body').append(savax.statusBar.$bar);
    for (var key in Drupal.settings.savax_status.items) {
      item = Drupal.settings.savax_status.items[key];
      if (item.enabled && Drupal.savax.statusBarItems[key]) {
        savax.statusBar.addItem(new Drupal.savax.statusBarItems[key](savax, item));
      }
    }
  },

  update: function(savax, property) {
    for (var key in savax.statusBar.items) {
      savax.statusBar.items[key].update(property);
    }
  }
};

/**
 * Extends Drupal.statusBarItem.
 * @abstract
 */
Drupal.savax.statusBarItem = function(savax, key, options) {
  this.savax = savax;
  Drupal.statusBarItem.call(this, key, options);
};
Drupal.savax.statusBarItem.prototype = new Drupal.statusBarItem();



/**
 * Message status bar item.
 * @constructor
 *
 * Extends Drupal.savax.statusBarItem.
 *
 * @param {Drupal.savax.form} savax
 */
Drupal.savax.statusBarItems.message = function(savax) {
  Drupal.savax.statusBarItem.call(this, savax, 'messages', Drupal.settings.savax_status.items.message);
};

Drupal.savax.statusBarItems.message.prototype = $.extend(new Drupal.savax.statusBarItem(), {

  /**
   * Implements Drupal.savax.statusBarItem.update
   */
  update: function(property) {
    if (property == 'last_modified') {
      return;
    }
    var savax = this.savax;
    if (!savax.enabled) {
      if (savax.halted) {
        this.setMessage(Drupal.t("Error: Auto save permanently disabled"), 'error');
      }
      else {
        this.setMessage(Drupal.t('Auto save has been disabled.'));
      }
    }
    else if (property == 'other_ajaxing') {
      this.setMessage(Drupal.t('Querying server …'));
    }
    else if (savax.ajax.ajaxing) {
      this.setMessage(Drupal.t('Saving …'));
    }
    else if (savax.last_status == 'validation') {
      this.setMessage(Drupal.t("Save failed. Please fix form errors."), 'warning');
    }
    else if (savax.dirty) {
      this.setMessage(Drupal.t('Modified.'));
    }
    else if (!savax.ajax.ajaxing && savax.last_status == 'success') {
      this.setMessage(Drupal.t('Saved.'));
      this.statusBar.hibernate();
    }
  },

  /**
   * Update content with given message
   *
   * @param {string} message
   *   The message to display
   * @param {string} level
   *   The severity of the message
   */
  setMessage: function(message, level) {
    this.statusBar.$bar.removeClass('savax-bar-error savax-bar-warning');
    if (level) {
      this.statusBar.$bar.addClass('savax-bar-' + level);
    }
    this.setContent(message);
  }
});

/**
 * Last saved status bar item.
 * @constructor
 *
 * Extends Drupal.savax.statusBarItem.
 *
 * @param {Drupal.savax.form} savax
 */
Drupal.savax.statusBarItems.lastSaved = function(savax) {
  var options = $.extend({
    label: Drupal.t('Last saved'),
    disturb: false
  }, Drupal.settings.savax_status.items.lastSaved);
  Drupal.savax.statusBarItem.call(this, savax, 'lastSaved', options);
  this.last_saved = savax.last_saved;
  this.setContent(this.toString());
};

Drupal.savax.statusBarItems.lastSaved.prototype = $.extend(new Drupal.savax.statusBarItem(), {

  /**
   * Implements Drupal.savax.statusBarItem.update
   */
  update: function(property) {
    if (this.last_saved < this.savax.last_saved) {
      this.last_saved = this.savax.last_saved;
      this.setContent(this.toString());
    }
    return false;
  },

  /**
   * Implements Object.toString()
   *
   * @return Last saved timestamp as string.
   */
  toString: function() {
    var date = new Date(this.savax.last_saved);
    if (Drupal.settings.savax_status.use_intl && window.Intl && typeof window.Intl === 'object' && window.Intl.DateTimeFormat) {
      var intl_options = { month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
      return new Intl.DateTimeFormat(Drupal.settings.savax_status.locale, intl_options).format(date);
    }
    return Drupal.savax.phpjs.date(Drupal.settings.savax_status.date_format, date);
  }
});

/**
 * Auto save enabled status bar item.
 * @constructor
 *
 * Extends Drupal.savax.statusBarItem.
 *
 * @param {Drupal.savax.form} savax
 */
Drupal.savax.statusBarItems.enabled = function(savax) {
  var options = $.extend({ label: Drupal.t('Auto-save') }, Drupal.settings.savax_status.items.enabled);
  Drupal.savax.statusBarItem.call(this, savax, 'enabled', options);
  this.enabled = savax.enabled;
  this.setContent(this.toString());
};

Drupal.savax.statusBarItems.enabled.prototype = $.extend(new Drupal.savax.statusBarItem(), {

  /**
   * Implements Drupal.savax.statusBarItem.update
   */
  update: function(property) {
    if (this.enabled != this.savax.enabled) {
      this.enabled = this.savax.enabled;
      this.setContent(this.toString());
    }
  },

  /**
   * Implements Object.toString()
   *
   * @return Last saved timestamp as string.
   */
  toString: function() {
    return this.savax.enabled ? Drupal.t('On') : Drupal.t('Off');
  }
});

})(jQuery);
