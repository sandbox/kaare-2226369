/**
 * @file
 * A generic status bar with auto hide capabilities.
 */

(function($) {

/**
 * The main status bar.
 *
 * One element must contain the class 'items-area' and cannot have further
 * children. They will nevertheless be overwritten.
 */
Drupal.theme.prototype.statusBar = function() {
  return '<div class="status-bar"><div class="status-bar-inner items-area"></div></div>';
};

/**
 * One status bar item.
 *
 * @param Object item
 *   An instance of Drupal.statusBarItem().
 */
Drupal.theme.prototype.statusBarItem = function(item) {
  return '<span class="bar-item ' + item.getClass() + '"></span>';
};

/**
 * A status bar item label.
 *
 * @param String label
 */
Drupal.theme.prototype.statusBarItemLabel = function(label) {
  return '<span class="bar-label">' + label + ': </span>';
};

/**
 * The actual content of a status bar item.
 *
 * @param String content
 */
Drupal.theme.prototype.statusBarItemContent = function(content) {
  return '<span class="bar-content">' + content + '</span>';
};

/**
 * Creates a generic status bar with arbitrary items.
 *
 * The status bar have to be added by the utilizing code, as with the items
 * within the status bar.
 */
Drupal.statusBar = function(options) {
  $.extend(this, {
    locale: '',
    autohide: true,
    delay_hide: 4000,
    hidden: false,
    timer_id: null
  }, options);
  this.items = {};
  this.$bar = $(Drupal.theme('statusBar'));
  if (this.id) {
    this.$bar.attr('id', this.id);
  }
  this.$itemsArea = this.$bar.find('.items-area');
};

Drupal.statusBar.prototype = {

  /**
   * Hides the status bar after a while.
   */
  hibernate: function() {
    if (!this.autohide) {
      return;
    }
    var statusBar = this;
    this.timeout = setTimeout(function() {
      statusBar.hidden = true;
      statusBar.$bar.slideUp();
    }, statusBar.delay_hide);
  },

  /**
   * Reveal the status bar if hidden.
   */
  wakeUp: function() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      delete this.timeout;
    }
    if (this.hidden) {
      this.hidden = false;
      this.$bar.show();
    }
  },

  /**
   * Add an item to the status bar.
   *
   * @param Object item
   *   An instance of Drupal.statusBarItem().
   */
  addItem: function(item) {
    var before = false,
        least = 10000,
        key;
    item.statusBar = this;
    for (key in this.items) {
      if (item.weight < this.items[key].weight && item.weight < least) {
        before = key;
        least = item.weight;
      }
    }
    if (before) {
      this.items[key].$container.before(item.$container);
    }
    else {
      this.$itemsArea.append(item.$container);
    }
    this.items[item.key] = item;
  },

  /**
   * Sets the content for a given status bar item.
   *
   * @param String key
   *   The status bar item to update content for
   * @param String content
   *   The value/content to replace the status bar item with.
   */
  setContent: function(key, content) {
    if (this.items[key]) {
      this.items[key].setContent(content);
    }
  }
};

/**
 * A single status bar item.
 *
 * @param String key
 *   The identifier of the item for this status bar.
 * @param Object options
 *   Configuration options for this item.
 */
Drupal.statusBarItem = function(key, options) {
  $.extend(this, {
    // The content to be displayed
    content: '',
    // Wake status bar from hibernation if raised
    disturb: true,
    // The label of this status bar item.
    label: '',
    // Weight of status bar item. Lower = left, higher = right.
    weight: 0
  }, options);

  this.key = key;
  this.$container = $(Drupal.theme('statusBarItem', this));
  if (this.label) {
    this.$container.append(Drupal.theme('statusBarItemLabel', this.label));
  }
  this.$content = $(Drupal.theme('statusBarItemContent', this.content));
  this.$container.append(this.$content);
};

Drupal.statusBarItem.prototype = {

  /**
   * The element class identifying this item in the status bar.
   */
  getClass: function() {
    return 'bar-item-' + this.key;
  },

  /**
   * Overwrites existing content/value for this status bar item.
   *
   * @param String content.
   */
  setContent: function(content) {
    if (content != this.content) {
      if (this.statusBar && this.disturb) {
        this.statusBar.wakeUp();
      }
      this.content = content;
      this.$content.html(content);
    }
  }
};

})(jQuery);
