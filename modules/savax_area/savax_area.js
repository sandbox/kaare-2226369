/**
 * @file
 * Ajax auto save status area.
 *
 * Minimalistisc version of the status bar, where only the textual message is displayed.
 */

;(function($) {

Drupal.savax = Drupal.savax || {};
Drupal.savax.behaviors = Drupal.savax.behaviors || {};

Drupal.savax.behaviors.savaxArea = {

  attach: function(savax) {
    savax.area = new Drupal.savax.area(savax);
  },

  update: function(savax, property) {
    if (property == 'last_modified') {
      return;
    }
    var area = savax.area;
    if (!savax.enabled) {
      if (savax.halted) {
        area.setMessage(Drupal.t("Error: Auto save permanently disabled"), 'error');
        area.setClass('savax-status-error');
      }
      else {
        area.setMessage(Drupal.t('Auto save has been disabled'));
        area.setClass('savax-status-disabled');
      }
    }
    else if (property == 'other_ajaxing') {
      area.setMessage(Drupal.t('Waiting for server …'));
      area.setClass('savax-status-saving');
    }
    else if (savax.ajax.ajaxing) {
      area.setMessage(Drupal.t('Saving …'));
      area.setClass('savax-status-saving');
    }
    else if (savax.last_status == 'validation') {
      area.setMessage(Drupal.t("Save failed. Please fix form errors."), 'warning');
      area.setClass('savax-status-warning');
    }
    else if (savax.dirty) {
      area.setMessage(Drupal.t('Modified'));
      area.setClass('savax-status-dirty');
    }
    else if (!savax.ajax.ajaxing && savax.last_status == 'success') {
      area.setMessage(Drupal.t('Saved'));
      area.setClass('savax-status-ok');
    }
  }
};

Drupal.savax.area = function(savax) {
  this.savax = savax;
  this.previous_class = '';
  this.$area = $('.savax-area');
};

Drupal.savax.area.prototype = {
  setMessage: function(msg) {
    this.$area.html(msg);
  },

  setClass: function(class_name) {
    this.$area.removeClass(this.previous_class);
    this.$area.addClass(class_name);
    this.previous_class = class_name;
  }
};

})(jQuery);
