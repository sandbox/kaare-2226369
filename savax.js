/**
 * @file
 * Performs auto save of nodes in Drupal.
 *
 * It utilizes and extends the Drupal ajax and states API introducing ways to
 * hook into savax. A new state 'autosave' is introduced and in PHP you can
 * trigger auto save on and off based on a remote condition like this:
 *
 * @code
 *   $form['example'] = array(
 *     '#type' => 'select',
 *     '#states' => array(
 *       'autosave' => array(
 *         'input[name="example2"]' => array('checked' => TRUE)
 *       ),
 *     ),
 *   );
 * @endcode
 *
 * Note: This will ignore existing auto save toggle based on node draft status.
 * If you also need to depend on that you have to massage and alter the
 * 'autosave' remote state condition in hook_form_node_form_alter(), also making
 * sure your node form hook fires after savax's.
 *
 * @code
 *   function example_form_node_form_alter(&$form, &$form_state, $form_id) {
 *     if (isset($form['actions']['savax_submit']['#states'])) {
 *       $condition = $form['actions']['savax_submit']['#states']['autosave'];
 *       ...
 *     }
 *   }
 * @endcode
 */

;(function($) {

Drupal.savax = Drupal.savax || {};
Drupal.savax.behaviors = Drupal.savax.behaviors || {};

// Our custom states API event
$(document).bind('state:autosave', function(e) {
  if (e.trigger) {
    var savax = $(e.target).closest('form').data('savax');
    if (! savax) {
      return;
    }
    e.value ? savax.enable() : savax.disable();
  }
});

Drupal.behaviors.savax = {
  attach: function(context, settings) {
    // Downstream. Search through all forms and see if there are savax settings
    // for it, in which case a new savax object is attached to it.
    $('form', context).once('savax-form').each(function() {
      var id = this.id;
      if (settings.savax.forms[id] == undefined) {
        return;
      }
      Drupal.savax.form[id] = new Drupal.savax.form($(this), settings.savax.forms[id]);
    });
    // Upstream. We need to listen to changes to newly created elements.
    var savax = $(context).closest('form').data('savax');
    if (savax) {
      savax.bindEvents(context);
    }
  }
};

/**
 * Overrides the default beforeSubmit handler for all ajax objects.
 *
 * Bump the other_ajaxing semaphore for the form's savax object when it's not
 * our own ajax object in action.
 */
Drupal.ajax.prototype.beforeSubmit = function(form_values, $form, options) {
  var savax = $form.data('savax');
  if (!savax || this.element.id === savax.$submit.attr('id')) {
    return;
  }
  savax.other_ajaxing++;
  savax.updateBehaviors('other_ajaxing');
};

/**
 * State and logic around auto saving a single form.
 * @constructor
 */
Drupal.savax.form = function($form, settings) {
  var now = new Date().getTime();
  var defaults = {
    // Raised if content is modified client side.
    dirty: false,
    // List of form elements with errors attached
    $error_items: {},
    // When the form initially was edited.
    first_modified: 0,
    // Auto save won't be enabled once this is raised.
    halted: false,
    // When was the form last edited.
    last_modified: now,
    // When did the last ajax save occur.
    last_saved: now,
    // Status of last save attempt
    last_status: 'success',
    // Elevation level of auto save. 'draft' is used during editing and
    // 'candidate' when input looses focus.
    level: 'draft',
    // Level of currently running submit
    level_submitted: '',
    // Raised if form has bin altered after last submit attempt
    modified_after_submit: false,
    // Semaphore of other ajax operations in progress.
    other_ajaxing: 0,
    // List of tableDrag objects modified.
    tabledrags_modified: {}
  };

  $.extend(this, defaults, settings);
  $form.data('savax', this);
  this.$form = $form;
  this.$submit = $form.find('#' + this.submit_id);
  this.bindEvents();
  this.ajax = Drupal.ajax[this.submit_id];

  var savax = this;

  /**
   * Invoke a method on the savax object.
   *
   * This is a custom ajax command as an extension to the drupal ajax api. From
   * php in your ajax handler invoked by
   * @code
   *   $commands[] = savax_ajax_command_invoke($method, $data);
   *   return array('#type' => 'ajax', '#commands' => $commands);
   * @endcode
   */
  this.ajax.commands.savaxInvoke = function(ajax, response, status) {
    if (savax[response.method]) {
      savax[response.method](response.argument);
    }
    else {
      alert(Drupal.t("Internal error: Unknown method '@method' in savax object.", {'@method': response.method }));
      savax.disable();
    }
  };

  /**
   * Implements Drupal.ajax.prototype.beforeSubmit()
   */
  this.ajax.beforeSubmit = function(form_values, element_settings, options) {
    savax.beforeSubmit(form_values, element_settings, options);
  };

  /**
   * Overrides Drupal.ajax.prototype.options.complete()
   */
  var ajax_complete = this.ajax.options.complete;
  this.ajax.options.complete = function(response, status) {
    var ret = ajax_complete(response, status);
    // ajax.ajaxing is updated in Drupal.ajax.prototype.options.complete, so we
    // have to run after it.
    savax.complete(response, status);
    return ret;
  };

  this.attachBehaviors();
  if (this.enabled) {
    this.enable();
  }
};

Drupal.savax.form.prototype = {

  /**
   * Allow listeners to act on newly created savax objects.
   */
  attachBehaviors: function() {
    for (var behavior in Drupal.savax.behaviors) {
      if (Drupal.savax.behaviors[behavior].attach) {
        Drupal.savax.behaviors[behavior].attach(this);
      }
    }
  },

  /**
   * Allow listeners to act on updated savax objects.
   *
   * @param {string} change
   *   Type of change that caused the update.
   */
  updateBehaviors: function(change) {
    for (var behavior in Drupal.savax.behaviors) {
      if (Drupal.savax.behaviors[behavior].update) {
        Drupal.savax.behaviors[behavior].update(this, change);
      }
    }
  },

  /**
   * Setup form input event listeners within context.
   *
   * @param {Object} context
   *   Optional. What context to bind events within. Elements found within this
   *   context is connected to 'this' object.
   */
  bindEvents: function(context) {
    context = context || this.$form;
    var savax = this;

    var blurEvent = function(event) {
      // Allow a potential 'beforeunload' event to execute before 'blur'.
      // Leaving a page executes a synchronous ajax request before it releases
      // the grip on the page, whereas this.saveCandidate() is async.
      setTimeout(function() { savax.saveCandidate(); }, 400);
    };
    this.ajaxListener();
    this.tableDragListener();

    // Bind the dirty trigger to all input elements for this form
    $('input[type="text"]:not(.form-autocomplete), input[type="password"], textarea', context)
      .once('savax-listen')
      .bind('keyup', this, Drupal.savax.keyupToChange)
      .bind('change', function(event) { savax.inputModified(); })
      .bind('blur', blurEvent);
    // Files are different. Managed files are uploaded through ajax, so we're
    // just triggering the ajax event for it. Once it's successfully uploaded
    // we're tagging the form as dirty.
    $('input[type="file"]', context)
      .once('savax-listen')
      .bind('change', function() { savax.fileUpload(this); });
    // The rest ...
    $('input, select', context).not('[type="hidden"], [type="submit"]')
      .once('savax-listen')
      .bind('change', function(event) { savax.inputModified(); })
      .bind('blur', blurEvent);
    $('html').once('savax-listen').each(function() {
      $(window).bind('beforeunload', function (event) { return savax.cleanup(); });
    });
    // Form submit buttions trigger 'beforeunload' events, causing
    // savax.cleanup() to be invoked. Let's disable auto save altogether for
    // these buttons.
    $('input[type="submit"]:not(.ajax-processed)', context).once('savax-listen').mouseup(function(event) {
      savax.disable();
    });
  },

  /**
   * Bind and react on all ajax complete callbacks.
   */
  ajaxListener: function() {
    var savax = this;

    for (var id in Drupal.ajax) {
      // We don't want to hijack our own ajax handler
      if (id === this.$submit.attr('id')) {
        continue;
      }

      $('#' + id, this.$form).once('savax-listen').each(function() {
        savax.ajaxCompleteHandler(Drupal.ajax[id]);
      });
    }
  },

  /**
   * Override the complete handler for given ajax object.
   *
   * Lower/release the other_ajaxing semaphore and mark form dirty.
   *
   * @param {Drupal.ajax} ajax
   *   The ajax instance we're attaching a complete handler for.
   */
  ajaxCompleteHandler: function(ajax) {
    var ajaxComplete = ajax.options.complete,
        savax = this;

    ajax.options.complete = function(response, status) {
      savax.other_ajaxing--;
      if (status == 'timeout' || status == 'error' || status == 'parsererror') {
        savax.last_status = 'error';
        // As with our own ajax complete handler we're halting on any kind of
        // errors. Any failed communications with the server is bad for us.
        savax.halt();
      }
      else {
        savax.inputModified();
        if (ajax.element.tagName == 'INPUT' && ajax.element.type == 'submit'
            && ajax.submit._triggering_element_name.match(/^field_.*_remove_button$/)) {
          // We're issuing a candidate save for multi value field delete
          // operations as this is an instant change in the form. Adding fields
          // assumes more modifications are required before any save is
          // necessary.
          setTimeout(function() { savax.saveCandidate(); }, 400);
        }
      }
      return ajaxComplete(response, status);
    };
  },

  /**
   * Listen to modifications to all draggable tables within savax form.
   */
  tableDragListener: function() {
    var id,
        savax = this;
    if (typeof Drupal.tableDrag == 'undefined') {
      return;
    }
    for (id in Drupal.tableDrag) {
      if (!$('#' + id, savax.$form).once('savax-listen').length) {
        continue;
      }
      Drupal.tableDrag[id].onDrop = function() {
        var tableDrag = this,
            id = this.table.id;
        if (tableDrag.changed) {
          savax.inputModified();
          if (typeof savax.tabledrags_modified[id] == 'undefined') {
            savax.tabledrags_modified[id] = tableDrag;
          }
          if (savax.enabled) {
            // Remove the warning issued by tabledrag.js
            $('#' + tableDrag.table.id).closest('.form-item').find('.tabledrag-changed-warning').remove();
          }
        }
      };
    }
  },

  /**
   * Remove all signs of draggable tables beeing modified.
   */
  cleanTableDrags: function() {
    var id, tableDrag;
    if (this.modified_after_submit) {
      // @todo: Tables NOT modified after submit should be cleaned up.
      return;
    }
    for (id in this.tabledrags_modified) {
      tableDrag = this.tabledrags_modified[id];
      $('span.tabledrag-changed', tableDrag.table).remove();
      $('tr.drag-previous', tableDrag.table).removeClass('drag-previous');
    }
    this.tabledrags_modified = {};
  },

  /**
   * Enable auto save for this form.
   */
  enable: function() {
    if (this.halted) {
      return;
    }
    if (!this.enabled || !this.interval_id) {
      this.enabled = true;
      this.interval_id = window.setInterval(Drupal.savax.timer, Drupal.settings.savax.timer_delay);
      this.$submit.removeAttr('disabled');
      this.updateBehaviors('enabled');
      // Hide stock submit button if configured to do so
      if (Drupal.settings.savax.visible_submit == 'visible_disabled') {
        $('.savax-stock-submit', this.$form).addClass('savax-hidden');
      }
    }
  },

  /**
   * Disable auto save for this form.
   */
  disable: function() {
    if (this.enabled) {
      if (this.interval_id != undefined) {
        window.clearInterval(this.interval_id);
      }
      this.$submit.attr('disabled', 'disabled');
      this.enabled = false;
      this.updateBehaviors('enabled');
      // Show stock submit button if configured to do so
      if (Drupal.settings.savax.visible_submit == 'visible_disabled') {
        $('.savax-stock-submit', this.$form).removeClass('savax-hidden');
      }
    }
  },

  /**
   * Update the savax object to indicate a save submission is in progress.
   */
  beforeSubmit: function() {
    this.level_submitted = this.level;
    this.$form.find('[name="savax_level"]').val(this.level);
    this.updateBehaviors('ajaxing');
  },

  /**
   * Indicates a successful auto save.
   *
   * This is never called from the savax object, but needs to be added as an
   * ajax command in PHP using the savax_ajax_command_invoke('success');
   * statement.
   */
  success: function() {
    var now = new Date().getTime(),
        label;
    // Workaround for https://drupal.org/node/736066
    this.$form.find('div > [name="changed"]').unwrap();
    this.last_saved = now;
    this.last_status = 'success';
    if (this.modified_after_submit) {
      this.first_modified = now;
      label = Drupal.settings.savax.label_unsaved;
    }
    else {
      this.dirty = false;
      label = Drupal.settings.savax.label_saved;
    }
    this.$submit.val(label);
    this.removeErrors();
    this.cleanTableDrags();
  },

  /**
   * Indicates a auto save fail due to validation errors.
   *
   * This is supposed to be invoked server-side using
   * savax_ajax_command_invoke('validationFailed')
   *
   * @param {Object} arguments
   *   Data from server-side.
   *
   * @see Drupal.savax.prototype.success().
   */
  validationFailed: function(arguments) {
    if (this.level_submitted == 'candidate') {
      var $form_item, class_name;
      for (class_name in arguments.error_map) {
        $form_item = $('.' + class_name, this.$form);
        $form_item.find(':input').not('[type="hidden"]').addClass('error');
        $form_item.find('.savax-form-error').remove();
        $form_item.append($('<div class="savax-form-error"></div>').append(arguments.error_map[class_name]));
        this.$error_items[class_name] = $form_item;
      }
      // Remove existing errors no longer present in error_map.
      for (class_name in this.$error_items) {
        if (arguments.error_map[class_name] == undefined) {
          this.removeError(class_name);
        }
      }
    }
    this.last_status = 'validation';
  },

  /**
   * Permanently halt auto save for this form.
   */
  halt: function() {
    this.halted = true;
    this.last_status = 'error';
    this.disable();
    // Allow editors to select text for text fields by making them readonly. All
    // other must be set disabled.
    $('input[type="text"],textarea', this.$form).once('savax-halted').attr('readonly', true);
    $('input, select').once('savax-halted').attr('disabled', true);
    this.$form.addClass('savax-halted');
  },

  /**
   * Ajax 'complete' callback.
   *
   * Reset savax object after a save request has ended, independent of request
   * status.
   */
  complete: function(response, status) {
    if (status != 'success' || !response.responseText) {
      this.halt();
    }
    if (this.modified_after_submit) {
      // We're starting the counter after save requests are complete to avoid
      // immediate auto saves when the requests takes really long time.
      this.first_modified = this.last_modified = new Date().getTime();
    }
    this.level_submitted = '';
    this.updateBehaviors('ajaxing');
  },

  /**
   * Last minute cleanup before window disappears.
   *
   * @return {mixed}
   *   Either nothing or a message to show the user before leaving the page.
   *   This is used as 'onbeforeunload' handler, and IE 9 will show a dialog
   *   window no matter what you return, even 'null'.
   */
  cleanup: function() {
    if (!this.enabled) {
      return;
    }
    if (!this.ajax.ajaxing) {
      if (this.dirty) {
        this.level = 'cleanup';
        // jquery.form.js uses iframes as default, causing submissions to be
        // asynchronous. ajax.options.async isn't respected unless we also omit
        // usage of iframe.
        this.ajax.options.iframe = false;
        this.ajax.options.async = false;
        this.save();
      }
      return;
    }
    else {
      return Drupal.t("There is an auto save in progress for unsaved changes on this page. If you leave this page those changes will be lost.");
    }
  },

  /**
   * Indicate that change were made to the form.
   *
   * Called whenever input elements are modified or other parts of the form
   * results in a different data set than stored server side.
   */
  inputModified: function() {
    var now = new Date().getTime();
    this.level = 'draft';
    this.last_modified = now;
    if (!this.modified_after_submit) {
      this.modified_after_submit = true;
      this.first_modified = now;
      if (!this.dirty) {
        this.dirty = true;
        this.$submit.val(Drupal.settings.savax.label_unsaved);
        this.updateBehaviors('dirty');
      }
    }
    else {
      this.updateBehaviors('last_modified');
    }
  },

  /**
   * Trigger ajax upload of managed file.
   *
   * @param {node} element
   *   input element of type 'file'.
   */
  fileUpload: function(element) {
    if (element.value.length > 0) {
      var $managed = $(element).closest('.form-managed-file');
      if ($managed.length) {
        // Find the associated upload button
        var $submit = $($managed).find('.form-submit'),
            id = $submit.attr('id');
        // Invoke the configured ajax event for the submit/upload button
        $submit.trigger(Drupal.ajax[id].event);
      }
      // @todo: Unmanaged files
    }
  },

  /**
   * Issue a 'candidate' save.
   *
   * When a form element is considered complete or ready, this is issued and
   * it will invoke an 'candidate' auto save request, escalating the
   * validation and error reporting from the server.
   */
  saveCandidate: function() {
    this.level = 'candidate';

    var now = new Date().getTime();
    if (now - this.last_saved > Drupal.settings.savax.delay_min) {
      this.save();
    }
    // If not saved, this.check() will have to do it for us.
  },

  /**
   * Check current form status and save it if timeout conditions are met.
   */
  check: function() {
    if (!this.enabled || !this.modified_after_submit) {
      return;
    }
    var now = new Date().getTime();
    if (now - this.first_modified > Drupal.settings.savax.delay_max) {
      // Max delay is reached. Forced save.
      this.save();
    }
    else if (now - this.last_modified > Drupal.settings.savax.delay_inactive) {
      // Inactive delay is reached.
      this.save();
    }
    else if (this.level == 'candidate' && now - this.last_saved > Drupal.settings.savax.delay_min) {
      // Dirty candidate saves should be saved as soon as delay_min is reached
      this.save();
    }
  },

  /**
   * Save this form through ajax.
   */
  save: function() {
    if (!this.enabled || !this.modified_after_submit || this.ajax.ajaxing || this.other_ajaxing) {
      return;
    }
    this.modified_after_submit = false;
    this.$form.find('[name="savax_level"]').val(this.level);
    try {
      this.ajax.form.ajaxSubmit(this.ajax.options);
    }
    catch (e) {
      this.ajax.ajaxing = false;
      alert("An error occurred while attempting to process " + this.ajax.options.url + ": " + e.message);
      this.updateBehaviors('ajaxing');
    }
  },

  /**
   * Remove a single form error
   *
   * @param {string} class_name
   *   Unique field item class name. Index in this.$error_elements
   */
  removeError: function(class_name) {
    if (this.$error_items[class_name] != undefined) {
      this.$error_items[class_name].find(':input').not('[type="hidden"]').removeClass('error');
      this.$error_items[class_name].find('.savax-form-error').remove();
      delete this.$error_items[class_name];
    }
  },

  /**
   * Remove all form errors
   */
  removeErrors: function() {
    for (var class_name in this.$error_items) {
      this.removeError(class_name);
    }
  }
};

/**
 * This function is called on every interval
 */
Drupal.savax.timer = function() {
  var savax;
  for (var i in Drupal.settings.savax.forms) {
    savax = $('#' + Drupal.settings.savax.forms[i].id).data('savax');
    if (!savax) {
      continue;
    }
    savax.check();
  }
};

/**
 * Trigger a 'change' event if key is likely to modify content.
 *
 * For textfields and textareas jQuery doesn't trigger a 'change' event until
 * it looses focus. This analyzes the given input key and determine whether
 * it's likely to modify the content, and finally fires a new 'change' event
 * if it does.
 */
Drupal.savax.keyupToChange = function(event) {
  if (event.isTrigger) {
    // If event is triggered somewhere else we handle it there. Ignore.
    return;
  }
  // Keys considered to be harmless. Copy paste from misc/autocomplete.js
  switch (event.keyCode) {
  case 9:  // Tab.
  case 16: // Shift.
  case 17: // Ctrl.
  case 18: // Alt.
  case 20: // Caps lock.
  case 27: // Esc.
  case 33: // Page up.
  case 34: // Page down.
  case 35: // End.
  case 36: // Home.
  case 37: // Left arrow.
  case 38: // Up arrow.
  case 39: // Right arrow.
  case 40: // Down arrow.
    return;
  }
  $(this).change();
};

})(jQuery);
