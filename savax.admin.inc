<?php

/**
 * @file
 * System wide settings for savax.module.
 */

/**
 * Form builder callback.
 */
function savax_system_settings_form() {
  $form['savax_admin'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => variable_get('savax_admin__active_tab', 'edit-timeout'),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'savax') . '/savax.admin.js'),
    ),
  );
  $form['timeout'] = array(
    '#type' => 'fieldset',
    '#title' => t("Time limits"),
    '#group' => 'savax_admin',
  );
  $form['timeout']['savax_delay_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Max delay'),
    '#size' => 20,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t("Maximum time in milliseconds after content has been modified to an auto save is forced."),
    '#default_value' => savax_conf('savax_delay_max'),
  );
  $form['timeout']['savax_delay_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Min delay'),
    '#size' => 20,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t("Minimum time in milliseconds between auto saves. Forms are staged for auto save when elements are changed on the blur event, meaning you could have a lot of auto save requests when users rapidly fills out several fields in a row. Use this setting to adjust the burden on the server."),
    '#default_value' => savax_conf('savax_delay_min'),
  );
  $form['timeout']['savax_delay_inactive'] = array(
    '#type' => 'textfield',
    '#title' => t('Inactive timeout'),
    '#size' => 20,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t("Time of inactivity in milliseconds since last modification to an auto save is performed. This can be used to detect the natural pauses editors have while editing content, and trigger an auto save during this pause. Increase to above <em>Max delay</em> to disable."),
    '#default_value' => savax_conf('savax_delay_inactive'),
  );

  $form['button_savax'] = array(
    '#type' => 'fieldset',
    '#title' => t("Ajax auto save button"),
    '#description' => t("Ajax auto save provides its own button to execute submits through ajax. Its visibility is toggled by CSS, but the button have to be present client side. If visible, it also serves as a primitive visual status of current form state."),
    '#group' => 'savax_admin',
  );
  $form['button_savax']['savax_visible_savax'] = array(
    '#type' => 'radios',
    '#title' => t("Visibility"),
    '#options' => array(1 => t('Visible'), 0 => t('Hidden')),
    '#default_value' => savax_conf('savax_visible_savax'),
  );
  $form['button_savax']['savax_label_saved'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for "Saved" status'),
    '#default_value' => savax_conf('savax_label_saved'),
    '#states' => array(
      'invisible' => array(
        ':input[name="savax_visible_savax"]' => array('value' => 0),
      ),
    ),
  );
  $form['button_savax']['savax_label_unsaved'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for "Unsaved" status'),
    '#default_value' => savax_conf('savax_label_unsaved'),
    '#states' => array(
      'invisible' => array(
        ':input[name="savax_visible_savax"]' => array('value' => 0),
      ),
    ),
  );

  $form['button_submit'] = array(
    '#type' => 'fieldset',
    '#title' => t("Save button"),
    '#description' => t("The behavior of the standard Save button is unchanged in Ajax auto save, but with auto save enabled it is sometimes useful to hide it. Warning: The <em>Hidden</em> state ignores whether auto save is enabled only for drafts mode or not."),
    '#group' => 'savax_admin',
  );
  $form['button_submit']['savax_visible_submit'] = array(
    '#type' => 'radios',
    '#title' => t("Visibility"),
    '#options' => array(
      'visible' => t('Visible'),
      'visible_disabled' => t('Visible when auto save is disabled'),
      'hidden' => t('Hidden'),
    ),
    '#default_value' => savax_conf('savax_visible_submit'),
  );

  $form['content_types'] = array(
    '#markup' => t("You explicitly have to enable auto save on some !content_types for these to have effect.", array(
      '!content_types' => l(t('content types'), 'admin/structure/types'),
    )),
  );

  return system_settings_form($form);
}
