<?php

/**
 * @file
 * Hooks provided by the Ajax auto save (savax) module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform changes to savax enabled (node) forms.
 *
 * @param array $settings
 *   Savax settings as submitted to client side (js).
 * @param array $form
 *   The form savax is enabled on.
 * @param array $form_state
 *   Form API form state.
 * @param string $form_id
 *   The form ID.
 */
function hook_savax_form_alter(array &$settings, array &$form, array &$form_state, $form_id) {
  // Disable savax for all forms.
  foreach ($settings['forms'] as $id => $form_settings) {
    $settings['forms'][$id]['enabled'] = FALSE;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
