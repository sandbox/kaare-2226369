/**
 * @file
 * Javascript for savax administration page.
 */

;(function($) {

Drupal.behaviors.savaxSystemSettings = {
  attach: function(context) {
    $('fieldset#edit-timeout', context).drupalSetSummary(function(context) {
      var vals = [];
      vals.push('Max: ' + ($('[name="savax_delay_max"]', context).val() / 1000) + 's');
      vals.push('Min: ' + ($('[name="savax_delay_min"]', context).val() / 1000) + 's');
      vals.push('Inactive: ' + ($('[name="savax_delay_inactive"]', context).val() / 1000) + 's');
      return vals.join(', ');
    });
    $('fieldset#edit-button-savax', context).drupalSetSummary(function(context) {
      return $('[name="savax_visible_savax"]:checked', context).val() == 1 ? 'Visible' : 'Hidden';
    });
    $('fieldset#edit-button-submit', context).drupalSetSummary(function(context) {
      return $('[name="savax_visible_submit"]:checked', context).closest('.form-item').find('label').html();
    });
  }
};

})(jQuery);
